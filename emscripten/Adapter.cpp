/*
* This file adapts  "m_s98.kpi v1.0r8 by Mamiya"  to the interface expected by my generic JavaScript player.
*
* Copyright (C) 2018-2023 Juergen Wothke
*
*
* Credits:
*
* The project is based on: http://www.vesta.dti.ne.jp/~tsato/soft_s98v3.html (by Mamiya, et al.)
* FM Sound Generator rel.008 (C) cisc 1998-2003.
* emu2413 by Mitsutaka Okazaki 2001-2004
* zlib (C) 1995-2005 Jean-loup Gailly and Mark Adler
*
* Notes:
*
* The sources currently available (above link or also the link here: http://www.purose.net/befis/download/lib/t98/ins98131s.zip
* all seem to point to outdated old versions and also the KBMediaPlayer plugin sources that I used here
* to NOT even match the plugin-binary that is provided on the same page: These sources doesn't compile before fixing a non existing
* enum reference. The sources date from 2006/10/19 whereas there has been some update of the winamp sources on 2011/05/31 -
* in case that update made any improvements on the emulator then these unfortunately will not be reflected here.
*
* License:
*
* NOT GPL: The code builds on FM Sound Generator with OPN/OPM interface Copyright (C) by cisc 1998, 2003.
* As the author of FM Sound Generator states, this is free software but for any commercial application
* the prior consent of the author is explicitly required.
* The same licence is here extended to any of the add-ons that are part of this project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <fstream>

#include <emscripten.h>

#include "s98types.h"
#include <m_s98.h>

#include "MetaInfoHelper.h"
using emsutil::MetaInfoHelper;


#define CHANNELS 2
#define RAW_INFO_MAX	1024


namespace s98 {

	class Adapter {
	public:
		Adapter() : _bufferLen(0), _sampleBuffer(NULL), _samplesAvailable(0), _bufferCopy(NULL), _s98File(NULL), _loopDetected(false)
		{
		}

		int loadFile(char* filename, void* inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: scopesEnabled support not implemented

			allocBuffers(audioBufSize);

			if (loadBuffer((uint8_t*)inBuffer, inBufSize, sampleRate))
			{
				extractStructFileInfo(filename);
				return 0;
			}
			return 1;	// error
		}

		void teardown()
		{
			trashBufferCopy();

			memset(&_s98Info, 0, sizeof(SOUNDINFO));

			MetaInfoHelper::getInstance()->clear();

			_loopDetected = false;

			if (_s98File)
			{
				delete _s98File;
				_s98File = 0;
			}
		}

		int getSampleRate()
		{
			return _s98Info.dwSamplesPerSec;
		}

		int genSamples()
		{
			if (_loopDetected) return 1;

			_samplesAvailable = _s98File->Write(_sampleBuffer, _bufferLen >> 2) ;
			return 0;
		}

		char* getSampleBuffer()
		{
			return (char*)_sampleBuffer;
		}

		long getSampleBufferLength()
		{
			return _samplesAvailable;
		}

		int getMaxPosition()
		{
			return _s98Info.dwSeekable ? _s98Info.dwLength : -1;
		}

		int getCurrentPosition()
		{
			if (_s98File->GetPosition() > _s98Info.dwLength)
			{
				_loopDetected = true;
			}
			return _s98File->GetPosition() % _s98Info.dwLength;
		}

		void seekPosition(int ms)
		{
			_s98File->SetPosition(ms);
		}
	private:
		std::string stringToUpper(std::string strToConvert)
		{
			std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

			return strToConvert;
		}

		void extractFromInfoLine(std::string line) {
			MetaInfoHelper *info = MetaInfoHelper::getInstance();
			std::string delimiter = "=";

			size_t pos = 0;
			if ((pos= line.find(delimiter)) != std::string::npos)
			{
				std::string key = stringToUpper(line.substr(0, pos));
				std::string value = line.substr(pos+1, line.length());

				if (!key.compare("TITLE"))
				{
					info->setText(0, value.c_str(), "");
				}
				else if (!key.compare("ARTIST"))
				{
					info->setText(1, value.c_str(), "");
				}
				else if (!key.compare("GAME"))
				{
					info->setText(2, value.c_str(), "");
				}
				else if (!key.compare("YEAR"))
				{
					info->setText(3, value.c_str(), "");
				}
				else if (!key.compare("GENRE"))
				{
					info->setText(4, value.c_str(), "");
				}
				else if (!key.compare("COMMENT"))
				{
					info->setText(5, value.c_str(), "");
				}
				else if (!key.compare("COPYRIGHT"))
				{
					info->setText(6, value.c_str(), "");
				}
				else if (!key.compare("S98BY"))
				{
					info->setText(7, value.c_str(), "");
				}
				else if (!key.compare("SYSTEM"))
				{
					info->setText(8, value.c_str(), "");
				}
			} else {
				fprintf(stderr, "garbage info: [%s]\n", line.c_str());
			}
		}

		void trashBufferCopy()
		{
			if (_bufferCopy)
			{
				free(_bufferCopy);
				_bufferCopy = NULL;
			}
		}

		uint8_t * getBufferCopy(uint8_t* inBuffer, size_t inBufSize)
		{
			trashBufferCopy();

			_bufferCopy = (uint8_t*)malloc(inBufSize);
			memcpy( _bufferCopy, inBuffer, inBufSize );
			return _bufferCopy;
		}

		void extractStructFileInfo(char *filename)
		{
			MetaInfoHelper *info = MetaInfoHelper::getInstance();

			_s98File->getRawFileInfo((uint8_t*)_rawInfoBuffer, RAW_INFO_MAX, 0);	// !_s98Info.dwIsV3

			if (strlen(_rawInfoBuffer) == 0)
			{
				// fallback: just use the filename
				std::string title = filename;
				title.erase( title.find_last_of( '.' ) );	// remove ext
				info->setText(0, title.c_str(), "");
			}
			else
			{
				/*
				note: V3 files contain tagged info, e.g.
				[S98]
				"title=Opening" 0x0a
				"artist=Yuzo Koshiro" 0x0a
				"game=Sorcerian" 0x0a
				"year=1987" 0x0a
				"genre=game" 0x0a
				"comment=This is sample data." 0x0a
				"copyright=Nihon Falcom" 0x0a
				"s98by=foo" 0x0a
				"system=PC-8801" 0x0a
				*/
				const char *pfx = "[S98]";
				int hasPrefix = !strncmp(_rawInfoBuffer, pfx, strlen(pfx));

				if (hasPrefix || _s98Info.dwIsV3)
				{
					std::string s= std::string(_rawInfoBuffer + (hasPrefix?strlen(pfx):0));

					std::string delimiter(1, (char)0xa);

					size_t pos = 0;
					std::string token;
					while ((pos = s.find(delimiter)) != std::string::npos) {
						token = s.substr(0, pos);
						extractFromInfoLine(token);
						s.erase(0, pos + delimiter.length());
					}
				}
				else
				{
					// some older format
					std::string in = _rawInfoBuffer;
					size_t p = in.find("Copyright");	// some contain this..

					if (p == std::string::npos)
					{
						// give up
						info->setText(0, in.c_str(), "");
					}
					else
					{
						// just split 2 sections
						const char *str = in.c_str();

						std::string encTitle = in.substr (0, p);
						info->setText(0, encTitle.c_str(), "");

						std::string encCopRigt = std::string(str + p);
						info->setText(6, encCopRigt.c_str(), "");
					}
				}
			}
		}

		void allocBuffers(int len)
		{
			if (len > _bufferLen)
			{
				if (_sampleBuffer) free(_sampleBuffer);

				_sampleBuffer = (int16_t*)malloc(sizeof(int16_t) * len * CHANNELS);
				_bufferLen = len;
			}
		}

		int loadBuffer(uint8_t* buffer, uint32_t size, uint32_t sampleRate )
		{
			// Emscripten passes its original cached data -- so we better use a copy..
			buffer = getBufferCopy(buffer, size);

			_s98File = new s98File();
			_s98Info.dwSamplesPerSec = sampleRate;

			return _s98File->OpenFromBuffer(buffer, size, &_s98Info);
		}

	private:
		int _bufferLen;
		int16_t* _sampleBuffer;;
		bool _loopDetected;

		char _rawInfoBuffer[RAW_INFO_MAX];
		int _samplesAvailable;
		uint8_t* _bufferCopy;

		// S98 stuff
		SOUNDINFO _s98Info;
		s98File *_s98File;
	};
};

s98::Adapter _adapter;


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func


EMBIND(int, emu_load_file(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return 0;	/*there are no subsongs*/ }
EMBIND(const char**, emu_get_track_info())			{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms))				{ _adapter.seekPosition(ms);}
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }
