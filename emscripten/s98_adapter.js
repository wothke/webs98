/*
 s98_adapter.js: Adapts S98 backend to generic WebAudio/ScriptProcessor player.

 version 1.1

 	Copyright (C) 2018-2023 Juergen Wothke

 LICENSE

 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version. This library is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/
class S98BackendAdapter extends EmsHEAP16BackendAdapter {
	constructor(modlandMode)
	{
		super(backend_S98.Module, 2, new SimpleFileMapper(backend_S98.Module));

		this.ensureReadyNotification();
	}
	
	loadMusicData(sampleRate, path, filename, data, options) {
		filename = this._getFilename(path, filename);

		let ret = this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), 1024, false);

		if (ret == 0)
		{
			this._setupOutputResampling(sampleRate);
		}
		return ret;
	}

	getSongInfoMeta()
	{
		return {
			title: String,		// the fields named in the V3 file spec
			artist: String,
			game: String,
			year: String,
			genre: String,
			comment: String,
			copyright: String,
			s98by: String,
			system: String,
		};
	}

	decodeText(t)
	{
		return this._decodeBinaryToText(t, "Shift_JIS", 256);
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		
		let numAttr = 9;
		let ret = this.Module.ccall('emu_get_track_info', 'number');

		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);
		result.title = this.decodeText(array[0]);
		result.artist = this.decodeText(array[1]);
		result.game = this.decodeText(array[2]);
		result.year = this.decodeText(array[3]);
		result.genre = this.decodeText(array[4]);
		result.comment = this.decodeText(array[5]);
		result.copyright = this.decodeText(array[6]);
		result.s98by = this.decodeText(array[7]);
		result.system = this.decodeText(array[8]);
	}
};

