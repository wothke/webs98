let songs = [
		"S98/Ymo HS/Dragon Master Silk 2/slkmsc25.s98",
		"S98/Tenpei Sato/Alvareak Adventure Story/42.s98",
		"S98/S. Suzuki/Reviver X1/reviver-x120.s98;;164",
		"S98/Ymo HS/Dragon Master Silk 2/slkmsc01.s98",
		"S98/Kenichi Arakawa/Jewel Gem Hunter Lime/opn-01.s98",
		"S98/Tadahiro Nitta/Xak/02-opening2.s98",
	];

class S98DisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 	{ return "webS98";}
	getDisplaySubtitle() 	{ return "NEC 98xx 音楽";}
	getDisplayLine1() { return this.getSongInfo().title; }
	getDisplayLine2() { return this.getSongInfo().artist+ (this.getSongInfo().artist.length?" ":"") + this.getSongInfo().game; }
	getDisplayLine3() { return this.getSongInfo().copyright+ (this.getSongInfo().copyright.length?" ":"") +this.getSongInfo().year; }
	// unused: genre, comment, s98by, system
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new S98BackendAdapter();

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					let arr = someSong.split(";");
					let track = arr.length > 1 ? parseInt(arr[1]) : -1;
					let timeout = arr.length > 2 ? parseInt(arr[2]) : -1;
					someSong = arr[0];
					// drag&dropped temp files start with "/tmp/"
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					let options= {};
					options.track = isNaN(track) ? -1 : track;
					options.timeout = isNaN(timeout) ? -1 : timeout;
					return [someSong, options];
				};

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new S98DisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x505050,0xffffff,0x404040,0xffffff], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}